//
//  FlashcardSetDetilViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/8/22.
//

import Foundation
import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var Tableview: UITableView!
    var sets2: [Flashcard] = [Flashcard]()
    
    @IBOutlet var Addbutton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = Flashcard()
        //connect hard coded collection to sets
        sets2 = Flashcard.getHardCodedFlashcards()
        Tableview.delegate = self
        Tableview.dataSource = self
    }

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return sets2.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Mycell", for: indexPath) as! FlashcardTableViewcell
    cell.Termcell.text = sets2[indexPath.row].term
    cell.Definitioncell.text = sets2[indexPath.row].definition
    return cell
}
    @IBAction func ab2(_ sender: Any) {
        
        let fc = Flashcard()
        fc.term = "Term 11"
        fc.definition = "Definition 11"
        sets2.append(fc)
        Tableview.reloadData()
        
    }
}
