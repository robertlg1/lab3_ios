//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet weak var CollectionView: UICollectionView!
    var sets: [FlashcardSet] = [FlashcardSet]()
    
    
    @IBOutlet var Addbutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = FlashcardSet()
        //connect hard coded collection to sets
        sets = FlashcardSet.getHardCodedCollection()
        CollectionView.delegate = self
        CollectionView.dataSource = self
        
    
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup cell display here
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
        
            }
    
    @IBAction func ab(_ sender: UIButton) {
        
        let fcs = FlashcardSet()
        fcs.title = "title 11"
        sets.append(fcs)
        CollectionView.reloadData()
    }
    
}
